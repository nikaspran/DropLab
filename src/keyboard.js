require('./window')(function(w){
  module.exports = function(){
    var currentKey;
    var currentFocus;
    var currentIndex = 0;
    var isUpArrow = false;
    var isDownArrow = false;
    var removeHighlight = function removeHighlight(list) {
      var listItems = list.list.querySelectorAll('li');
      listItems.forEach(function(li){
        li.classList.remove('dropdown-active');
      });
      return listItems;
    };

    var setMenuForArrows = function setMenuForArrows(list) {
      var listItems = removeHighlight(list);
      if(currentIndex>0){
        if(!listItems[currentIndex-1]){
          currentIndex = currentIndex-1; 
        }
        listItems[currentIndex-1].classList.add('dropdown-active');
      }
    };

    var mousedown = function mousedown(e) {
      var list = e.detail.hook.list;
      removeHighlight(list);
      list.show();
      currentIndex = 0;
      isUpArrow = false;
      isDownArrow = false;
    };
    var selectItem = function selectItem(list) {
      var listItems = removeHighlight(list);
      var currentItem = listItems[currentIndex-1];
      var listEvent = new CustomEvent('click.dl', {
        detail: {
          list: list,
          selected: currentItem,
          data: currentItem.dataset,
        },
      });
      list.list.dispatchEvent(listEvent);
      list.hide();
    }

    var keydown = function keydown(e){
      var typedOn = e.target;
      isUpArrow = false;
      isDownArrow = false;

      if(e.detail.which){
        currentKey = e.detail.which;
        if(currentKey === 13){
          selectItem(e.detail.hook.list);
          return;
        }
        if(currentKey === 38) {
          isUpArrow = true;
        }
        if(currentKey === 40) {
          isDownArrow = true;
        }
      } else if(e.detail.key) {
        currentKey = e.detail.key;
        if(currentKey === 'Enter'){
          selectItem(e.detail.hook.list);
          return;
        }
        if(currentKey === 'ArrowUp') {
          isUpArrow = true; 
        }
        if(currentKey === 'ArrowDown') {
          isDownArrow = true;
        }
      }
      if(isUpArrow){ currentIndex--; }
      if(isDownArrow){ currentIndex++; }
      if(currentIndex < 0){ currentIndex = 0; }
      setMenuForArrows(e.detail.hook.list);
    };

    w.addEventListener('mousedown.dl', mousedown);
    w.addEventListener('keydown.dl', keydown);
  };
});